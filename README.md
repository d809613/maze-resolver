# JS Solution for NAB Code Challenge

This solution takes an array as parameter and logs path to console. Below are the functions used for this solution:

* findPos(maze, pos) - returns pos in format [y, x] of given symbol

* findPath(maze) - returns an array of pos if path found or undefined otherwise

Note only the most basic test data are included due to the shortage of time.

### Assumptions
* maze always have S symbol to marks its start point
* maze always have F symbol to marks its finish point
* maze always have * symbol to marks its finish point
* maze always have empty space symbol to marks its finish point
* input maze is always a 2D array
* input maze always have start and finish points

### Prerequisites

* Javascript ES6

### Executing

Clone repo to your local system:

```
git clone https://d809613@bitbucket.org/d809613/maze-resolver.git
```

cd to the project folder:

```
cd maze-resolver
```

Executing the code in a JS runtime such as CodePen

## Testing

Basic test maze inputs are provided.

