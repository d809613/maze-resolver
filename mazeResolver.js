const maze1 = [
  ['#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#'],
  ['S', ' ', '#', ' ', ' ', ' ', '#', ' ', ' ', ' ', '#'],
  ['#', ' ', '#', ' ', '#', ' ', '#', ' ', '#', ' ', '#'],
  ['#', ' ', ' ', ' ', '#', ' ', ' ', ' ', '#', ' ', '#'],
  ['#', '#', '#', '#', '#', '#', '#', '#', '#', ' ', '#'],
  ['#', ' ', '#', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '#'],
  ['#', ' ', '#', ' ', '#', '#', '#', '#', '#', '#', '#'],
  ['#', ' ', '#', ' ', ' ', ' ', '#', ' ', ' ', ' ', '#'],
  ['#', ' ', '#', ' ', '#', ' ', '#', '#', '#', ' ', '#'],
  ['#', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', 'F'],
  ['#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#']
];

const maze2 = [
  ['#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#'],
  ['S', ' ', '#', ' ', ' ', ' ', '#', ' ', ' ', ' ', '#'],
  ['#', ' ', '#', ' ', '#', ' ', '#', ' ', '#', ' ', '#'],
  ['#', ' ', ' ', ' ', '#', ' ', ' ', ' ', '#', ' ', '#'],
  ['#', '#', '#', '#', '#', '#', '#', '#', '#', ' ', '#'],
  ['#', ' ', '#', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '#'],
  ['#', ' ', '#', ' ', '#', '#', '#', '#', '#', '#', '#'],
  ['#', ' ', '#', ' ', ' ', ' ', '#', ' ', ' ', ' ', '#'],
  ['#', ' ', '#', ' ', '#', ' ', '#', '#', '#', ' ', '#'],
  ['#', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', '#', 'F'],
  ['#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#']
];

const findPos = (maze, symbol) => {
  let pos;
  for (let i = 0; i < maze.length; i++) {
    for (let j = 0; j < maze[i].length; j++) {
      if (maze[i][j] === symbol) { pos = [i, j] }
    }
  }

  return pos;
};

const findPath = (maze) => {
  const start = findPos(maze, 'S');
  const end = findPos(maze, 'F');
  let queue = [];
  maze[start[0]][start[1]] = '#';
  queue.push([start]); // store a path, not just a position

  while (queue.length > 0) {
    const path = queue.shift(); // get the path out of the queue
    const pos = path[path.length-1]; // ... and then the last position from it
    const direction = [
      [pos[0] + 1, pos[1]], // down
      [pos[0], pos[1] + 1], // right
      [pos[0] - 1, pos[1]], // up
      [pos[0], pos[1] - 1]  // left
    ];

    for (let i = 0; i < direction.length; i++) {
      // find a path:
      if (direction[i][0] == end[0] && direction[i][1] == end[1]) {
        // return the path that led to the find
        return path.concat([end]);
      }

      if (direction[i][0] < 0 || // out of boundary
          direction[i][0] >= maze[0].length || // out of boundary
          direction[i][1] < 0 || // out of boundary
          direction[i][1] >= maze[0].length ||  // out of boundary
          maze[direction[i][0]][direction[i][1]] != ' ') { // hit wall
        // skip this iteration
        continue;
      }

      // mark cell to # to indicate it's visited
      maze[direction[i][0]][direction[i][1]] = '#';
      // extend and push the path on the queue
      queue.push(path.concat([direction[i]]));
    }
  }
};

// path found
console.log(findPath(maze1));
// path not found
console.log(findPath(maze2));

